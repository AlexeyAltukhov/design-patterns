package adapter;

public class USB implements ComputerDeviceIO{

    @Override
    public String read() {
        return "Information from USB";
    }
}
