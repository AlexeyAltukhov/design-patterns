package adapter;

public class Computer {

    public String getInfoFromComputerDeviceIO(ComputerDeviceIO device) {
        return device.read();
    }
}
