package adapter;

public class AdapterSD implements ComputerDeviceIO {

    protected SD sd;

    public AdapterSD(SD sd) {
        this.sd = sd;
    }

    @Override
    public String read() {
        return sd.getInformation();
    }
}
