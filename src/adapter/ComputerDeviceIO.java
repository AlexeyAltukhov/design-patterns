package adapter;

public interface ComputerDeviceIO {

    public String read();
}
