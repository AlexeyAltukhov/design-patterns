package adapter;

public class Main {

    public static void main(String[] args) {
        Computer computer = new Computer();
        ComputerDeviceIO usb = new USB();

        System.out.println(computer.getInfoFromComputerDeviceIO(usb));

        SD sd = new SD();
        AdapterSD adapterSD = new AdapterSD(sd);

        System.out.println(computer.getInfoFromComputerDeviceIO(adapterSD));
    }
}
