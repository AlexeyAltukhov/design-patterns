package factory_method;

public class Main {

    public static void main(String[] args) {
        Coffee espresso = CoffeeFactory.getCoffee(CoffeeType.ESPRESSO, 300, 2, 70);
        Coffee americano = CoffeeFactory.getCoffee(CoffeeType.AMERICANO, 200, 0, 90);

        System.out.println("Coffee ordered: " + espresso);
        System.out.println("Coffee ordered: " + americano);

        System.out.println("espresso instanceof Espresso: " + (espresso instanceof Espresso));
        System.out.println("americano instanceof Americano: " + (americano instanceof Americano));
    }
}
