package factory_method;

public class CoffeeFactory {

    public static Coffee getCoffee(CoffeeType coffeeType, double volume, double numberSugarCubes, double temperature) {
        switch (coffeeType) {
            case ESPRESSO:
                return new Espresso(volume, numberSugarCubes, temperature);
            case AMERICANO:
                return new Americano(volume, numberSugarCubes, temperature);
            default:
                return null;
        }
    }
}
