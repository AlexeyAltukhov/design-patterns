package factory_method;

public enum CoffeeType {
    ESPRESSO,
    AMERICANO
}
