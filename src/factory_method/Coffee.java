package factory_method;

public abstract class Coffee {

    public abstract double getVolume();

    public abstract double getNumberSugarCubes();

    public abstract double getTemperature();

    @Override
    public String toString() {
        return "volume = " + this.getVolume() + ", number sugar cubes = " + this.getNumberSugarCubes() +
                ", temperature = " + this.getTemperature();
    }
}
