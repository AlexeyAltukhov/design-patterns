package factory_method;

public class Espresso extends Coffee{

    private double volume;
    private double numberSugarCubes;
    private double temperature;

    public Espresso(double volume, double numberSugarCubes, double temperature) {
        this.volume = volume;
        this.numberSugarCubes = numberSugarCubes;
        this.temperature = temperature;
    }

    @Override
    public double getVolume() {
        return this.volume;
    }

    @Override
    public double getNumberSugarCubes() {
        return this.numberSugarCubes;
    }

    @Override
    public double getTemperature() {
        return this.temperature;
    }
}
