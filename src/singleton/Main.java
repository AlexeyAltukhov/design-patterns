package singleton;

import java.math.BigDecimal;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        logger.addToLog(BigDecimal.class);
        System.out.println(logger.getLog());
        logger.addToLog(Arrays.class);
        System.out.println(logger.getLog());
        logger.addToLog(Math.class);
        System.out.println(logger.getLog());
    }
}
