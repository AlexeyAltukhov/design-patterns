package singleton;

public class Logger {

    private static Logger instance;
    private static String log = "log info: ";

    private Logger() {

    }

    public synchronized static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void addToLog(Class<?> className) {
        if (log.length() > 10)
            log += ", ";
        log += className.getSimpleName();
    }

    public String getLog() {
        return log;
    }
}
