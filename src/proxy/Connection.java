package proxy;

public class Connection {

    private String nameDatabase = "my_db";
    private int port = 8080;

    public String getNameDatabase() {
        return nameDatabase;
    }

    public int getPort() {
        return port;
    }
}
