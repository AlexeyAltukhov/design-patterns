package proxy;

public class Main {

    public static void main(String[] args) {
        Connection connection = new Connection();
        Proxy proxy = new Proxy(connection);
        System.out.println(proxy.getNameDatabase());
    }
}
