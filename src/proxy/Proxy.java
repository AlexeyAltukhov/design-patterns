package proxy;

public class Proxy {

    Connection connection;

    public Proxy(Connection connection) {
        this.connection = connection;
    }

    public String getNameDatabase() {
        return "Connected to localhost:" + connection.getPort() + "/" + connection.getNameDatabase();
    }
}
